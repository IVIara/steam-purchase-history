import sys
import re
from bs4 import BeautifulSoup

if len(sys.argv) > 1:
	url = sys.argv[1]
else:
	url = input("Rentrez le chemin de votre historique d'achat Steam \nSteam > détails du compte > Voir l'historique des achats > Clic droit > afficher le code source > enregistrer\n")

soup = BeautifulSoup(open(url),'html.parser',from_encoding="iso-8859-1")

account_name = soup.title.string.split()[-1]

result = []
sum = 0.0
credit = []
cred_sum = 0.0
p = re.compile('\d+,[\d-]*')

tab = soup.find('table', class_='wallet_history_table').find_all(class_="wht_total")
cred = soup.find('table', class_='wallet_history_table').find_all(class_="wht_total wht_refunded")
date = soup.find('table', class_='wallet_history_table').find_all(class_="wht_date")

startdate = date[-1]

for c in cred:
	credit.append(p.findall(c.text))
	credit[-1] = credit[-1][0].replace('-','0')
	credit[-1] = credit[-1].replace(',','.')
	cred_sum += float(credit[-1])

for t in tab:
	if t is not None:
		if 'â‚¬' in t.text:
			result.append(p.findall(t.text))
			result[-1] = result[-1][0].replace('-','0')
			result[-1] = result[-1].replace(',','.')
			sum += float(result[-1])
		if 'CrÃ©ditÃ©' in t.text:
			credit.append(p.findall(t.text))
			credit[-1] = credit[-1][0].replace('-','0')
			credit[-1] = credit[-1].replace(',','.')
			cred_sum += float(credit[-1])

total = sum - 2 * cred_sum

print(account_name + " : " + str(round(total, 2)) + "€ depuis le " + startdate.text)
